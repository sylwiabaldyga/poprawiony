require 'calabash-android/calabash_steps'

Then /^I swipe left on ViewPager$/ do
  perform_action('drag',99,1,50,50,5)
end


Then /^I swipe right on ViewPager$/ do
  perform_action('drag',1,99,50,50,5)
end
