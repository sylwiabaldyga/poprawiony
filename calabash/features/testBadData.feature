Feature: Test bad data 

	Scenario: Bad date of birth
		Given I press the "Rodzic" button
		Then I enter text "09" into field with id "TextDay"
		Then I enter text " " into field with id "TextMonth"
		Then I enter text " " into field with id "TextYear"
		Given I press the "GOTOWE" button

	Scenario: Bad date of birth
		Given I press the "Rodzic" button
		Then I enter text " " into field with id "TextDay"
		Then I enter text "5" into field with id "TextMonth"
		Then I enter text " " into field with id "TextYear"
		Given I press the "GOTOWE" button



