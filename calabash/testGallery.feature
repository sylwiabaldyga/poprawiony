Feature: Test gallery

	Scenario: Pictures for children aged 3-6 months
		Given I press the "Rodzic" button
		Then I enter text "09" into field with id "TextDay"
		Then I enter text "02" into field with id "TextMonth"
		Then I enter text "2016" into field with id "TextYear"
		Given I press the "GOTOWE" button
		Given I press the "Obrazki" button
		Then I swipe left on ViewPager
		Then I swipe left on ViewPager
		Then I swipe right on ViewPager
		Then I swipe right on ViewPager


	Scenario: Feeding for children aged 3-6 months
		Given I press the "Rodzic" button
		Given I press the "Karmienie" button
		
