package com.example.sylwia.mobile_nanny.view.activity;


import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Server { //tcp
        public static final String TAG = Server.class.getSimpleName();
        private ServerSocket socket;
        private int port=5555;
        Connect connect;
        private OnReciveMessage onReciveMessage;

//    public void sendByte(byte[] image) {
//        connect.sendImage(image);
//    }

    public void sendByteArray(byte[] jdata, int length) {
        connect.sendImage(jdata,length);
    }

    public interface OnReciveMessage{
            void onRecive(String message);

        }
        public Server(OnReciveMessage onReciveMessage) {
            this.onReciveMessage=onReciveMessage;
        }


        public void start() {
            Thread thread= new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        socket=new ServerSocket(port);  
                        listen();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
        }

        private void listen() {

            Thread thread= new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "Czekam na polaczenie");
                    try {
                        Socket socket1 = socket.accept();
                        connect = new Connect(socket1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
        }


        class Connect {
            private DataInputStream inputStream;
            private DataOutputStream outputStream;

            public Connect(Socket socket){
                try {
                    inputStream=new DataInputStream(socket.getInputStream());
                    outputStream= new DataOutputStream(socket.getOutputStream());
                    Thread thread= new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String wiadomosc="";
                            boolean t=true;
                            while(t){
                                try {
                                    wiadomosc=inputStream.readUTF();
                                    Log.d(TAG,wiadomosc);
                                    onReciveMessage.onRecive(wiadomosc);
                                }
                                catch (SocketException | EOFException e) {
                                    t = false;
                                }
                                catch (IOException e) {
                                    t = false;
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                    thread.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }



//            public void sendImage(byte[] image) {
//                try {
//                    outputStream.writeInt(image.length);
//                    outputStream.write(image);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }

            public void sendImage(byte[] jdata, int length) {
                try {
                    outputStream.writeInt(length);
                    outputStream.write(jdata);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }




    }
