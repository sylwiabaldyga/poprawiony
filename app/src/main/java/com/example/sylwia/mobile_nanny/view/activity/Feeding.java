package com.example.sylwia.mobile_nanny.view.activity;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ImageView;

import com.example.sylwia.mobile_nanny.R;
import com.squareup.picasso.Picasso;

public class Feeding extends AppCompatActivity {
    private int ageChild;
    private WebView webView;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feeding);
        getAge(getIntent().getExtras());
        grapUi();
        loadHeaderImage();
        init();
        setHTML();

    }

    private void setHTML() {
        if(ageChild<6) {
            webView.loadUrl("file:///android_asset/zero-sixMonth.html");
        }else if(ageChild==6){
            webView.loadUrl("file:///android_asset/sixMonth.html");
        }else if(ageChild==7){
            webView.loadUrl("file:///android_asset/sevenMonth.html");
        }else if(ageChild==8){
            webView.loadUrl("file:///android_asset/eightMonth.html");
        }else if(ageChild==9){
            webView.loadUrl("file:///android_asset/nineMonth.html");
        }else if(ageChild==10){
            webView.loadUrl("file:///android_asset/tenMonth.html");
        }else if(ageChild==11){
            webView.loadUrl("file:///android_asset/elevenMonth.html");
        }else if(ageChild==12){
            webView.loadUrl("file:///android_asset/oneYear.html");
        }
    }

    private void init() {
        webView.getSettings().setJavaScriptEnabled(true); //modul javascrit potrzebny do funkcji pobierania int z androida do html
        webView.addJavascriptInterface(new WebAppInterface(getApplication(),1), "Android");
    }

    private void loadHeaderImage() {
        Picasso.with(getApplicationContext())
                .load("http://www.osesek.pl/files/styles/content/public/niemowle-noworodek-opieka-rutyna.jpg?itok=wqokDu5r")
                .into(image);
    }

    private void getAge(Bundle extras) {
        if (extras != null) {
            ageChild=extras.getInt("age");
        }
    }

    private void grapUi() {
        image= (ImageView)findViewById(R.id.image);
        webView =(WebView)findViewById(R.id.webView);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_feeding, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
