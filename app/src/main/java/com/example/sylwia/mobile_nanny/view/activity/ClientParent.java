package com.example.sylwia.mobile_nanny.view.activity;


import android.util.Log;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

public class ClientParent {

    public static final String TAG = ClientParent.class.getSimpleName();
    private Socket socket;
    private int port=5555;
    private Connect connect;
    private OnCameraImageListener listener;

    public void setListener(OnCameraImageListener listener) {
        this.listener = listener;
    }

    public ClientParent(final OnConnectClient onConnectClient ,final String ip){

        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    socket=new Socket(ip,port);
                    connect=new Connect(socket);
                    onConnectClient.onConnect();
                } catch (IOException e) {
                    e.printStackTrace();
                    onConnectClient.onNotConnect();
                }
            }
        });
        thread.start();

    }

    public void sendMessage(String s) {
        if(connect!=null){
            connect.sendMessage(s);
        }
    }



    class Connect {
        private DataInputStream inputStream;
        private DataOutputStream outputStream;

        public Connect(Socket socket){
            try {
                inputStream=new DataInputStream(socket.getInputStream());
                outputStream= new DataOutputStream(socket.getOutputStream());
                Thread thread= new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int length;
                        byte[] image;
                        boolean t=true;
                        while(t){
                            try {
                                length = inputStream.readInt();
                                image = new byte[length];
                                inputStream.readFully(image);
                                if (listener != null) {
                                    listener.onCameraImage(image);
                                }
                                Log.d("Client", "wielkosc obrazu " + length);
                            }
                            catch (SocketException | EOFException e) {
                                t = false;
                            }
                            catch (IOException e) {
                                t = false;
                                e.printStackTrace();
                            }
                        }
                    }
                });
                thread.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void sendMessage(String widomosc) {
            try {
                outputStream.writeUTF(widomosc);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    public interface OnCameraImageListener {
        void onCameraImage(byte[] image);
    }
}
