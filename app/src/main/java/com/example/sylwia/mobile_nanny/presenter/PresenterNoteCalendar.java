package com.example.sylwia.mobile_nanny.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.example.sylwia.mobile_nanny.model.DatabaseGreenDao;
import com.example.sylwia.mobile_nanny.model.MyApplication;
import com.example.sylwia.mobile_nanny.model.dataBase.NoteCalendar;
import com.example.sylwia.mobile_nanny.model.dataBase.NoteCalendarDao;

import java.util.ArrayList;
import java.util.List;

public class PresenterNoteCalendar {
    private List<NoteCalendar> list;
    private List<NoteCalendar> newList;
   // private DatabaseGreenDao greenDao;
    private List<NoteCalendar> deleteNotes;

    public PresenterNoteCalendar(Context context) {
        list = new ArrayList<>();
        newList = new ArrayList<>();
       // greenDao = new DatabaseGreenDao(context);
        deleteNotes = new ArrayList<>();
    }

    public List<NoteCalendar> getList() {
        return list;
    }

    public List<NoteCalendar> getNewList() {
        return newList;
    }

    public void save(Context context) {
        MyApplication application= (MyApplication)context;
        application.getDatabases().save(newList);
        application.getDatabases().removeNotes(deleteNotes);
    }


    public void downloadList(int month, int day,Context context){
        MyApplication application= (MyApplication)context;
        list=application.getDatabases().getNotesForDayInMonth(month, day);
    }


    public void remove(int position) {
        if(position<list.size()) {
            deleteNotes.add(list.get(position));
            list.remove(position);
        }else
            newList.remove(position);
    }
}
