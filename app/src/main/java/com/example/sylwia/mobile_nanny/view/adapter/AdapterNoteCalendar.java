package com.example.sylwia.mobile_nanny.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sylwia.mobile_nanny.R;
import com.example.sylwia.mobile_nanny.model.dataBase.NoteCalendar;

import java.util.List;

public class AdapterNoteCalendar extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<NoteCalendar> list;
    private List<NoteCalendar> newList;

    public AdapterNoteCalendar(List<NoteCalendar> list, List<NoteCalendar> newList){
        this.list = list;
        this.newList = newList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v1 = inflater.inflate(R.layout.note_item_list, viewGroup, false);
        viewHolder = new ItemViewHolder(v1);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ItemViewHolder itemViewHolder = (ItemViewHolder)viewHolder;
        if(i<list.size())
            itemViewHolder.itemImage.setText(list.get(i).getNote());
        else
            itemViewHolder.itemImage.setText(newList.get(i - list.size()).getNote());
    }

    @Override
    public int getItemCount() {
        return list.size()+ newList.size();
    }
    //region ViewHolder
    public class ItemViewHolder extends RecyclerView.ViewHolder {


        private TextView itemImage;

        public ItemViewHolder(View itemView) {
            super(itemView);
            itemImage = (TextView) itemView.findViewById(R.id.noteText);

        }

    }

}
