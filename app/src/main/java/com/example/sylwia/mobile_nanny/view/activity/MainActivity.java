package com.example.sylwia.mobile_nanny.view.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.sylwia.mobile_nanny.R;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void parent(View view) {
        Intent intent= new Intent(getApplicationContext(),Parent.class);
        startActivity(intent);
    }

    public void child(View view) {
        Intent intent= new Intent(getApplicationContext(),Child.class);
        startActivity(intent);
    }



}
