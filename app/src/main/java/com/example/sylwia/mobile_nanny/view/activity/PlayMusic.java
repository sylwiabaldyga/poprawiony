package com.example.sylwia.mobile_nanny.view.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sylwia.mobile_nanny.R;
import com.example.sylwia.mobile_nanny.presenter.PresenterPlayMusic;

public class PlayMusic extends AppCompatActivity {
    private ListView list ;
    private int Cliked = -1;
    private PresenterPlayMusic presenter;
    private ArrayAdapter adapter = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_music);
        presenter= new PresenterPlayMusic(getApplicationContext());


        grapUi();
        makeAdapter();
        setListener();
    }

    private void setListener() {
        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id){
                Toast.makeText(getBaseContext(), "Clicked" + position, Toast.LENGTH_SHORT).show();
                Cliked=position;
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void makeAdapter() {
        String music[] = {"Bach","EliseBeethoven","Vivaldi"};
        adapter = new ArrayAdapter(getApplication(), android.R.layout.simple_list_item_1, music){
            @Override
            public View getView(int possition,View conwert, ViewGroup parent){
                View view = super.getView(possition,conwert,parent);
                TextView text= (TextView)view.findViewById(android.R.id.text1);
                text.setTextColor(Color.BLUE);
                text.setBackgroundResource(R.drawable.my_selector); //kolor tla zaznaczonego elementu
                return view;
            }

        };

        list.setAdapter(adapter);
    }

    private void grapUi() {
        list = (ListView) findViewById(R.id.listView);
    }


    public void pause(View view) {

    }

    public void play(View view) {
        if (Cliked >= 0) {
            presenter.play("0" + Cliked);
        }
    }

    public void stop(View view) {
        presenter.stop("1");

    }
}
