package com.example.sylwia.mobile_nanny.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import com.example.sylwia.mobile_nanny.model.CalendarMonthInfo;
import com.example.sylwia.mobile_nanny.view.fragment.FragmentCalendar;

import java.util.ArrayList;
import java.util.List;

public class AdapterViewPagerCalendar extends FragmentStatePagerAdapter{

    private List<FragmentCalendar> list;


    public AdapterViewPagerCalendar(FragmentManager fm,int Months) {
        super(fm);
        list = new ArrayList<>();
        for(int i=0;i<Months+1;i++) {
            list.add(new FragmentCalendar());
        }
    }


    public Fragment getItem(int position) {
        list.get(position).setId(String.valueOf(position));
        return list.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        if (position == list.size()-3) {
            list.add(new FragmentCalendar());
            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {  //ile mam obrazkow(dlugosc fragmentu)
        return list.size();
    }

    public void showDays(CalendarMonthInfo numberDays, int currentPage, CalendarMonthInfo numberDaysMonthNext, CalendarMonthInfo numberDaysMonthPrev) {
        list.get(currentPage).setDays(numberDays);
        if(currentPage+1<list.size()) {
            list.get(currentPage + 1).setDays((numberDaysMonthNext));
        }
        if(currentPage!=0){
            list.get(currentPage-1).setDays((numberDaysMonthPrev));
        }
    }

    public int clickOnDay(int position){
        return list.get(position).clickOnDay();
    }

    public void updateViewMonth(int currentItem) {
        FragmentCalendar fr = list.get(currentItem);
        fr.setDaywithDatabases(currentItem);
    }
}
