package com.example.sylwia.mobile_nanny.view.activity;


import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.*;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.sylwia.mobile_nanny.presenter.PresenterServer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ViewCamera extends SurfaceView implements SurfaceHolder.Callback {

    private android.hardware.Camera camera;
    private SurfaceHolder surfaceHolder;
    private SendImage sendImage;
    private boolean send = false;

    public ViewCamera(Context context, Camera camera, final SendImage sendImage) {
        super(context);
        this.sendImage = sendImage;
        this.camera=camera;
        this.camera.setDisplayOrientation(90);
        Camera.Parameters parameters = camera.getParameters();
        parameters.setPictureSize(10, 10);
        parameters.setPreviewFrameRate(5);
        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        }catch (IOException e ){
            Log.d("Exeption"," start Preview" );
        }
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if(surfaceHolder.getSurface()==null){
            return;
        }
        try {
            camera.stopPreview();
        }catch (Exception e){

        }

        try {
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        }catch (IOException e ){
            Log.d("Exeption"," start Preview" );
        }
    }



    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        camera.stopPreview();
        camera.release();
    }

    public void sendImage() {
        Camera.Size sizePreview=camera.getParameters().getPreviewSize();
        int dataBufferSize=(int)(sizePreview.height*sizePreview.width*
                (ImageFormat.getBitsPerPixel(camera.getParameters().getPreviewFormat())/8.0));
        camera.addCallbackBuffer(new byte[dataBufferSize]);
        camera.setPreviewCallbackWithBuffer(new Camera.PreviewCallback() {

            public synchronized void onPreviewFrame(byte[] data, Camera camera) {

                Camera.Size sizePreview = camera.getParameters().getPreviewSize();
                YuvImage yuvimage=new YuvImage(data, ImageFormat.NV21, sizePreview.width, sizePreview.height, null);
                ByteArrayOutputStream baoStream = new ByteArrayOutputStream();
                yuvimage.compressToJpeg(new Rect(0, 0, sizePreview.width, sizePreview.height), 80, baoStream);
                byte[] jdata = baoStream.toByteArray();
                sendImage.send(jdata,jdata.length);

                try{
                    camera.addCallbackBuffer(data);
                }catch (Exception e) {
                    Log.e("Camera", " error with CallbackBuffer");
                    return;
                }
                return;
            }
        });
    }

    public void stopImage() {
        send = false;
        camera.setPreviewCallbackWithBuffer(null);
    }

}
