package com.example.sylwia.mobile_nanny.view.fragment;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sylwia.mobile_nanny.R;
import com.example.sylwia.mobile_nanny.model.CalendarMonthInfo;
import com.example.sylwia.mobile_nanny.presenter.PresenterFragmentCalendar;


public class FragmentCalendar extends Fragment implements PresenterFragmentCalendar.OnFragmentCalendarListener {

    private TextView[] textListener;
    private String tag;
    private PresenterFragmentCalendar presenter;
    private CalendarMonthInfo days;


    public int clickOnDay(){

        return presenter.getClcickedDay();

    }

    public void setId(String tag) {
        this.tag = tag;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = (PresenterFragmentCalendar)getActivity().getSupportFragmentManager().findFragmentByTag(tag);
        if (presenter==null) {
            presenter = new PresenterFragmentCalendar();
            presenter.setMonth(Integer.parseInt(tag));
            presenter.setListener(this);
            getActivity().getSupportFragmentManager().beginTransaction().add(presenter, tag).commit();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_fragment_calendar, container, false);
        createCellsView(view);
        setBackgroundClickedCells();
        update();
        showDaysWithDatabase();
        return  view;
    }

    private void update() {
        setDays(days);
    }

    private void setBackgroundClickedCells() {
        if(presenter.getClcickedDay()!=-1) {
            textListener[presenter.getClcickedDay()].setBackgroundColor(Color.BLUE);
        }
    }


    private void createCellsView(View view) {
        LinearLayout lLayout=getMyLayout(view);
        LinearLayout linearLayout=new LinearLayout(getActivity().getApplicationContext());
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
        params=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
        linearLayout.setLayoutParams(params);
        LinearLayout.LayoutParams params1= new LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT);
        textListener=new TextView[42];
        for(int j=0;j<6;j++) {
            linearLayout=setRows();
            for (int i = 0; i < 7; i++) {
                setColumns(linearLayout,i,j);
            }
            lLayout.addView(linearLayout);
        }
    }

    private void setColumns(LinearLayout linearLayout, int i, int j) {
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
        int index=j * 7 + i;
        textListener[index] = new TextView(getActivity().getApplicationContext());
        textListener[index].setText(String.valueOf(index+ 1));
        textListener[index].setGravity(Gravity.CENTER);
        if (!presenter.isDataInDay(index))
            textListener[index].setBackground(getActivity().getResources().getDrawable(R.drawable.gradient_button));
        else
            textListener[index].setBackground(getActivity().getResources().getDrawable(R.drawable.selected_button));
        params1.weight = 1;
        textListener[index].setLayoutParams(params1);
        linearLayout.addView(textListener[index]);
        setListenerButton(index);
    }

    private LinearLayout setRows() {
        LinearLayout linearLayout = new LinearLayout(getActivity().getApplicationContext());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0);
        params.weight = 1;
        linearLayout.setLayoutParams(params);
        return linearLayout;

    }

    private LinearLayout getMyLayout(View view) {
        LinearLayout lLayout=(LinearLayout) view.findViewById(R.id.calendarView);
        lLayout.setOrientation(LinearLayout.VERTICAL);
        return lLayout;
    }

    public void setListenerButton(final int index) {

        textListener[index].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(presenter.getClcickedDay()!=-1 && presenter.getClcickedDay() != index) {
                    if(presenter.isDataInDay(presenter.getClcickedDay()))
                        textListener[presenter.getClcickedDay()].setBackground(getActivity().getResources().getDrawable(R.drawable.selected_button));
                    else {
                        textListener[presenter.getClcickedDay()].setBackground(getActivity().getResources().getDrawable(R.drawable.gradient_button));
                    }
                }
                presenter.setClcickedDay(index);
                textListener[index].setBackgroundColor(Color.BLUE);
            }
        });
    }

    public void setDays(CalendarMonthInfo numberDays) {
        if (days == null) {
            days = numberDays;
        }
        if (textListener != null && days!=null) {
            int count = 0;
            for (int i = 0; i < 42; i++) {
                if (days.getFirstDay() <= i && count < days.getCountDays()) {
                    textListener[i].setText(String.valueOf(count + 1));
                    textListener[i].setVisibility(View.VISIBLE);
                    count++;
                } else {
                    textListener[i].setVisibility(View.INVISIBLE);
                }

            }
        }

    }

    public void setDaywithDatabases(int currentItem) {
        presenter.setDaysWithData(currentItem);

    }

    @Override
    public void showDaysWithDatabase() {
        if(textListener!=null && days != null){
            int count = 0;
            for (int i = 0; i < 42; i++) {
                if (days.getFirstDay() <= i && count < days.getCountDays()) {
                    textListener[i].setText(String.valueOf(count + 1));
                    textListener[i].setVisibility(View.VISIBLE);
                    if(presenter.isDataInDay(i))
                        textListener[i].setBackground(getActivity().getResources().getDrawable(R.drawable.selected_button));
                    else {
                        textListener[i].setBackground(getActivity().getResources().getDrawable(R.drawable.shape_calendar_day));
                    }
                    count++;
                } else {
                    textListener[i].setVisibility(View.INVISIBLE);
                }

            }
        }
    }
}
