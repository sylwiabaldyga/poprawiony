package com.example.sylwia.mobile_nanny.model;


import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import com.example.sylwia.mobile_nanny.model.dataBase.DaoMaster;
import com.example.sylwia.mobile_nanny.model.dataBase.DaoSession;
import com.example.sylwia.mobile_nanny.model.dataBase.NoteCalendar;
import com.example.sylwia.mobile_nanny.model.dataBase.NoteCalendarDao;

import java.util.List;

public class DatabaseGreenDao {

    private NoteCalendarDao noteCalendarDao;

    public DatabaseGreenDao(Context context){
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context,
                "notes-db", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        noteCalendarDao = daoSession.getNoteCalendarDao();
    }
    public void addNote(int month, int day, String note){
        try {
            noteCalendarDao.insert( new NoteCalendar(null, month, day, note));
        } catch (SQLiteConstraintException e) {
            throw e;
        }

    }

    public void removeNote(int month,int day, String note){
        try {
            noteCalendarDao.delete(new NoteCalendar(null, month, day, note));
        } catch (SQLiteConstraintException e) {
            throw e;
        }
    }

    public List<NoteCalendar> getNoteForMonth(int month){
        return noteCalendarDao.queryBuilder()
                .where(NoteCalendarDao.Properties.NumberMonth.eq(month))
                .list();
    }

    public List<NoteCalendar> getNotesForDayInMonth(int month, int day){
        return noteCalendarDao.queryBuilder()
                .where(NoteCalendarDao.Properties.NumberMonth.eq(month),
                        NoteCalendarDao.Properties.NumberDay.eq(day))
                .list();
    }

    public void save (List<NoteCalendar> notes) {
        if (notes !=null && notes.size() > 0)
            noteCalendarDao.insertInTx(notes);

    }

    public void removeNotes(List<NoteCalendar> removeNotes) {
        if (removeNotes !=null && removeNotes.size() > 0)
        noteCalendarDao.deleteInTx(removeNotes);
    }

    public void clearDatabases(){
        noteCalendarDao.deleteAll();
    }
}
