package com.example.sylwia.mobile_nanny.view.activity;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;



public class OnRecyclerViewListener implements RecyclerView.OnItemTouchListener {




    public interface OnClickListenerRecycler {
        public void onLongClick(View view, int position);
        void onClick(int position, View view);

    }
    private GestureDetector detector;
    private OnClickListenerRecycler onListe;

    public OnRecyclerViewListener(Context context, OnClickListenerRecycler listener, final RecyclerView recyclerView) {
        onListe = listener;
        detector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent event) {
                return true;
            }

            @Override
            public void onLongPress(MotionEvent event) {
                View ViewChildUnder = recyclerView.findChildViewUnder(event.getX(), event.getY());

                if (ViewChildUnder != null && onListe != null) {
                    onListe.onLongClick(ViewChildUnder, recyclerView.getChildAdapterPosition(ViewChildUnder));
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent event) {
        View ViewChildUnder = view.findChildViewUnder(event.getX(), event.getY());
        if (ViewChildUnder != null && onListe != null && detector.onTouchEvent(event)) {
            onListe.onClick( view.getChildAdapterPosition(ViewChildUnder),ViewChildUnder);
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}

