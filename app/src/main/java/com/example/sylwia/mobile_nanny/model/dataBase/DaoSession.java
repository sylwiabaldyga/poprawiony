package com.example.sylwia.mobile_nanny.model.dataBase;

import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.AbstractDaoSession;
import de.greenrobot.dao.identityscope.IdentityScopeType;
import de.greenrobot.dao.internal.DaoConfig;

import com.example.sylwia.mobile_nanny.model.dataBase.NoteCalendar;

import com.example.sylwia.mobile_nanny.model.dataBase.NoteCalendarDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see de.greenrobot.dao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig noteCalendarDaoConfig;

    private final NoteCalendarDao noteCalendarDao;

    public DaoSession(SQLiteDatabase db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        noteCalendarDaoConfig = daoConfigMap.get(NoteCalendarDao.class).clone();
        noteCalendarDaoConfig.initIdentityScope(type);

        noteCalendarDao = new NoteCalendarDao(noteCalendarDaoConfig, this);

        registerDao(NoteCalendar.class, noteCalendarDao);
    }
    
    public void clear() {
        noteCalendarDaoConfig.getIdentityScope().clear();
    }

    public NoteCalendarDao getNoteCalendarDao() {
        return noteCalendarDao;
    }

}
