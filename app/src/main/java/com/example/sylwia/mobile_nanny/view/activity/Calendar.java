package com.example.sylwia.mobile_nanny.view.activity;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.sylwia.mobile_nanny.R;
import com.example.sylwia.mobile_nanny.model.CalendarMonthInfo;
import com.example.sylwia.mobile_nanny.presenter.PresenterCalendar;
import com.example.sylwia.mobile_nanny.view.adapter.AdapterViewPagerCalendar;

public class Calendar extends AppCompatActivity implements PresenterCalendar.OnCalendarListener {

    private ViewPager MonthSelected;
    private AdapterViewPagerCalendar adaperViewPagercalendar;
    private TextView currentMonth;
    private int Months;
    private PresenterCalendar presenterCalendar;
    private int currentPage=Months-1;
    private boolean buttonChangedMonth;
    public static final int portForResults=10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        init();
        getAge(getIntent().getExtras());
        grapUi();
        setAdapterCalendar();
        setCurrentDate();
        setListener();
    }

    private void setListener() {
        MonthSelected.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                int leterCurrentPage = currentPage;
                currentPage=position;
                if (leterCurrentPage<position && !buttonChangedMonth ){
                    presenterCalendar.nextShowMonth(getApplicationContext());
                }else if(leterCurrentPage > position && !buttonChangedMonth){
                    presenterCalendar.prevShowMonth(getApplicationContext());
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setCurrentDate() {
        MonthSelected.setCurrentItem(Months - 1);
        presenterCalendar.setCurrentDate(getApplicationContext());
    }

    private void init() {
        buttonChangedMonth=false;
        Months=0;
        presenterCalendar = new PresenterCalendar(this);
    }

    private void setAdapterCalendar() {
        adaperViewPagercalendar= new AdapterViewPagerCalendar(getSupportFragmentManager(),Months);
        MonthSelected.setAdapter(adaperViewPagercalendar);

    }

    private void grapUi() {
        MonthSelected = (ViewPager)findViewById(R.id.viewPagerCalendar);
        currentMonth = (TextView)findViewById(R.id.currentMonth);
    }

    private void getAge(Bundle extras) {
        if (extras != null) {
            Months=extras.getInt("age");
            currentPage = Months-1;
        }
    }

    public void prevMonth(View view) {
        if(MonthSelected.getCurrentItem()!=0) {
            buttonChangedMonth=true;
            MonthSelected.setCurrentItem(MonthSelected.getCurrentItem() - 1);
            presenterCalendar.prevShowMonth(getApplicationContext());
        }
    }

    public void nextMonth(View view) {
        buttonChangedMonth=true;
        MonthSelected.setCurrentItem(MonthSelected.getCurrentItem()+1);
        presenterCalendar.nextShowMonth(getApplicationContext());
    }



    @Override
    public void showDate(String date) {
        currentMonth.setText(date);
        buttonChangedMonth=false;
    }

    @Override
    public void showDaysInMonth(CalendarMonthInfo numberDays,
                                CalendarMonthInfo numberDaysMonthNext,
                                CalendarMonthInfo numberDaysMonthPrev) {
        adaperViewPagercalendar.showDays(numberDays,currentPage
                ,numberDaysMonthNext,numberDaysMonthPrev);
    }

    public void addNote(View view) {
        if(adaperViewPagercalendar.clickOnDay(MonthSelected.getCurrentItem())!=-1) {
            Intent intent = new Intent(getApplicationContext(), NoteCalendar.class);

            intent.putExtra("month",MonthSelected.getCurrentItem());
            intent.putExtra("day", adaperViewPagercalendar.clickOnDay(MonthSelected.getCurrentItem()));
            startActivityForResult(intent,portForResults);
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == portForResults) {
            if (resultCode == RESULT_OK) {
               if(data!=null){
                   if(data.getExtras().getInt("Save data",0)==1){
                       adaperViewPagercalendar.updateViewMonth(MonthSelected.getCurrentItem()); // pobieram  nr page miesiac  z fragmentu -on wie ktory dzien
                   }
               }
            }
        }
    }
}
