package com.example.sylwia.mobile_nanny.util;

import android.content.Context;
import android.widget.Switch;

import com.example.sylwia.mobile_nanny.R;

public class Constant {

    public static String getNameMonth(int numberMonth, Context context){
        String number="";
        switch (numberMonth){
            case 0 :
                number=context.getString(R.string.styczen);
                break;
            case 1 :
                number=context.getString(R.string.luty);
                break;
            case 2 :
                number=context.getString(R.string.marzec);
                break;
            case 3 :
                number=context.getString(R.string.kwiecien);
                break;
            case 4 :
                number=context.getString(R.string.maj);
                break;
            case 5 :
                number=context.getString(R.string.czerwiec);
                break;
            case 6 :
                number=context.getString(R.string.lipiec);
                break;
            case 7 :
                number=context.getString(R.string.sierpien);
                break;
            case 8 :
                number=context.getString(R.string.wrzesien);
                break;
            case 9 :
                number=context.getString(R.string.pazdziernik);
                break;
            case 10 :
                number=context.getString(R.string.listopad);
                break;
            case 11 :
                number=context.getString(R.string.grudzien);
                break;
        }
     return number;
    }
    
    public static int getDaysinMonth(int month, int year){
        int number=0;
        switch (month){
            case 0 :
                number=31;
                break;
            case 1 :
                if(year%4==0 && year%400!=0)
                    number=29;
                else
                    number=28;
                break;
            case 2 :
                number=31;
                break;
            case 3 :
                number=30;
                break;
            case 4 :
                number=31;
                break;
            case 5 :
                number=30;
                break;
            case 6 :
                number=31;
                break;
            case 7 :
                number=31;
                break;
            case 8 :
                number=30;
                break;
            case 9 :
                number=31;
                break;
            case 10 :
                number=30;
                break;
            case 11 :
                number=31;
                break;
        }
        
        
        return number;
    }


}
