package com.example.sylwia.mobile_nanny.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.example.sylwia.mobile_nanny.R;
import com.example.sylwia.mobile_nanny.model.MyApplication;

public class Nanny extends AppCompatActivity implements OnConnectClient {
    private String ip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nanny);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ip= extras.getString("ip");
        }
        MyApplication application= (MyApplication)getApplicationContext();
        application.startClient(ip,this);
    }


    public void lullabyy(View view) {
        Intent intent= new Intent(this,PlayMusic.class);
        intent.putExtra("ip",ip);
        startActivity(intent);
    }

    public void viewCamera(View view) {
        Intent intent = new Intent(getApplicationContext(), Camera.class);
        startActivity(intent);
    }

    @Override
    public void onConnect() {

    }

    @Override
    public void onNotConnect() {

    }
}
