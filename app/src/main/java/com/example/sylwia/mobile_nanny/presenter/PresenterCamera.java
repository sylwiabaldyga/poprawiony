package com.example.sylwia.mobile_nanny.presenter;

import android.content.Context;

import com.example.sylwia.mobile_nanny.model.MyApplication;
import com.example.sylwia.mobile_nanny.view.activity.Camera;
import com.example.sylwia.mobile_nanny.view.activity.ClientParent;
import com.example.sylwia.mobile_nanny.view.activity.SendImage;

public class PresenterCamera {

    public ClientParent client;

    public PresenterCamera(Context context) {
        MyApplication application = (MyApplication) context;
        client = application.getClient();
    }

    public void getImage() {
        client.sendMessage("image");
    }

    public void stopImage() {
        client.sendMessage("stopImage");
    }

    public void setCameraListener(ClientParent.OnCameraImageListener camera) {
        client.setListener(camera);
    }
}
