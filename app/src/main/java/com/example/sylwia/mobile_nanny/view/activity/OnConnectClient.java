package com.example.sylwia.mobile_nanny.view.activity;


public interface OnConnectClient {

    public void onConnect();
    public void onNotConnect();
}
