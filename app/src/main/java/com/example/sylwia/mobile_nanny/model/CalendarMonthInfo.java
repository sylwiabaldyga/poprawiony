package com.example.sylwia.mobile_nanny.model;


public class CalendarMonthInfo {

    private int firstDay;
    private int countDays;

    public CalendarMonthInfo(int countDays, int firstDay) {
        this.firstDay = firstDay;
        this.countDays = countDays;
    }

    public int getFirstDay() {
        return firstDay;
    }

    public int getCountDays() {
        return countDays;
    }

}
