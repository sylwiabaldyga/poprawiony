package com.example.sylwia.mobile_nanny.presenter;

import android.content.Context;

import com.example.sylwia.mobile_nanny.model.MyApplication;
import com.example.sylwia.mobile_nanny.view.activity.ClientParent;

public class PresenterPlayMusic {

    private ClientParent client;

    public PresenterPlayMusic(Context context) {
        MyApplication application = (MyApplication) context;
        client = application.getClient();
    }

    public void stop(String message) {
        client.sendMessage(message);
    }

    public void play(String musicId) {
        client.sendMessage(musicId);
    }
}
