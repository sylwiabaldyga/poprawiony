package com.example.sylwia.mobile_nanny.view.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.sylwia.mobile_nanny.R;
import com.squareup.picasso.Picasso;


public class FragmentGalleryViewPager extends Fragment {

    private ImageView picture;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_fragment_gallery_view_pager, container, false);
        picture=(ImageView)view.findViewById(R.id.picture);
        Picasso.with(getActivity()).load(adressPicture).into(picture);


        return view;
    }

    private int adressPicture;

    public void setAdressPicture(int adressPicture) {
        this.adressPicture = adressPicture;
    }

}
