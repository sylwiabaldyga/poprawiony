package com.example.sylwia.mobile_nanny.view.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.sylwia.mobile_nanny.R;
import com.example.sylwia.mobile_nanny.presenter.PresenterCamera;

public class Camera extends AppCompatActivity implements ClientParent.OnCameraImageListener {

    private PresenterCamera presenter;
    private ImageView imageCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        imageCamera = (ImageView) findViewById(R.id.imageCamera);
        imageCamera.setRotation(90);
        presenter = new PresenterCamera(getApplicationContext());
        presenter.setCameraListener(this);
        presenter.getImage();

    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.setCameraListener(null);
        presenter.stopImage();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setCameraListener(this);
        presenter.getImage();
    }

    @Override
    public void onCameraImage(byte[] image) {
        final Bitmap bmp = BitmapFactory.decodeByteArray(image, 0, image.length);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imageCamera.setImageBitmap(bmp);
            }
        });
    }
}
