package com.example.sylwia.mobile_nanny.view.activity;


public interface OnDialogListener {
        void delete(int position);
        void add(String message);
        void changeData(String day,String month,String year);
        void changeIp(String ip);

}
