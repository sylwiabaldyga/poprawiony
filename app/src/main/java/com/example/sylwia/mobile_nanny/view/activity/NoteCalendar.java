package com.example.sylwia.mobile_nanny.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.sylwia.mobile_nanny.R;
import com.example.sylwia.mobile_nanny.presenter.PresenterNoteCalendar;
import com.example.sylwia.mobile_nanny.view.adapter.AdapterNoteCalendar;

public class NoteCalendar extends AppCompatActivity implements OnDialogListener {
    private RecyclerView recyclerView;
    private FloatingActionButton floatingActionButton;
    private AdapterNoteCalendar adapter;
    private DialogRemove dialogR;
    private DialogAdd dialogA;
    private int month;
    private int day;
    private PresenterNoteCalendar presenterNoteCalendar;
    private int onBackPressed = 0;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_calendar);
        initPresenter();
        getBundle(getIntent().getExtras());
        downloadNotesDatabases();
        grapUi();
        init();
        setListener();

    }

    private void downloadNotesDatabases() {
        presenterNoteCalendar.downloadList(month, day,getApplicationContext());
    }

    private void initPresenter() {
        if(presenterNoteCalendar == null){
            presenterNoteCalendar = new PresenterNoteCalendar(getApplicationContext());
        }
    }

    private void getBundle(Bundle extras) {
        month = extras.getInt("month");
        day = extras.getInt("day");
    }

    private void setListener() {
        final OnDialogListener listener = this;
        recyclerView.addOnItemTouchListener(new OnRecyclerViewListener(getApplicationContext(), new OnRecyclerViewListener.OnClickListenerRecycler() {
            @Override
            public void onClick( int position,View view) {
            }

            @Override
            public void onLongClick(View view, int position) {
                if (dialogR == null) {
                    dialogR = new DialogRemove();
                    dialogR.setListener(listener);
                }
                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                dialogR.setArguments(bundle);
                dialogR.show(getSupportFragmentManager(), "dialogR");
            }
        }, recyclerView));


        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialogA == null) {
                    dialogA = new DialogAdd();
                    dialogA.setOnDialogListener(listener);
                }
                dialogA.show(getSupportFragmentManager(), "dialogA");
            }
        });
    }

    private void init() {

        recyclerView.setHasFixedSize(true); //Improves performance if recView is not going to change its size.
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        adapter = new AdapterNoteCalendar(presenterNoteCalendar.getList(),presenterNoteCalendar.getNewList());
        recyclerView.setAdapter(adapter);
    }

    private void grapUi() {
        recyclerView = (RecyclerView) findViewById(R.id.noteList);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.actionButton);
    }

    @Override
    public void delete(int position) {
        presenterNoteCalendar.remove(position);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void add(String message) {
        presenterNoteCalendar.getNewList().add(new com.example.sylwia.mobile_nanny.model.dataBase.NoteCalendar(null, month, day, message));
        adapter.notifyDataSetChanged();
    }

    @Override
    public void changeData(String day, String month, String year) {

    }

    @Override
    public void changeIp(String ip) {

    }

    @Override
    public void onBackPressed() {
        presenterNoteCalendar.save(getApplicationContext());
        if ( presenterNoteCalendar.getList().size() > 0 || presenterNoteCalendar.getNewList().size() >0){
            onBackPressed = 1;
            Intent result = new Intent();
            result.putExtra("Save data", 1);
            setResult(Activity.RESULT_OK, result);
            finish();
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if (onBackPressed == 0) {
            presenterNoteCalendar.save(getApplicationContext());
        }


        super.onDestroy();
    }
}