package com.example.sylwia.mobile_nanny.view.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.sylwia.mobile_nanny.R;


public class AdapterGallery extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private TypedArray list;
    private Context context;
    public AdapterGallery(Context context, TypedArray list){
        this.context=context;
        this.list=list;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v1 = inflater.inflate(R.layout.image_view_item, viewGroup, false);
        viewHolder = new ItemViewHolder(v1);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ItemViewHolder itemViewHolder = (ItemViewHolder)viewHolder;
        itemViewHolder.itemImage.setImageResource(list.getResourceId(i, -1));
    }

    @Override
    public int getItemCount() {
        return list.length();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {


        private ImageView itemImage;

        public ItemViewHolder(View itemView) {
            super(itemView);
            itemImage = (ImageView) itemView.findViewById(R.id.image);

        }

    }
    //endregion
}