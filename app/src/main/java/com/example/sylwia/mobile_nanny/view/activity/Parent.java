package com.example.sylwia.mobile_nanny.view.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sylwia.mobile_nanny.R;
import com.example.sylwia.mobile_nanny.model.MyApplication;
import com.example.sylwia.mobile_nanny.presenter.PresenterParent;



public class Parent extends AppCompatActivity implements PresenterParent.OnPresenterParentListener, OnDialogListener{

    private EditText ipAdress;
    private EditText TextDay,TextMonth,TextYear;
    private Button buttonOk;
    private Button gallery;
    private TextView AgeChild;
    private LinearLayout date;
    private PresenterParent presenterParent;
    private CoordinatorLayout coordinator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent);
        init();
        grapUi();
        setListener();
        checkData();
        checkIp();
    }

    private void checkData() {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.savebirthday), Context.MODE_PRIVATE);
        int day = sharedPref.getInt(getString(R.string.day), 0);
        int month = sharedPref.getInt(getString(R.string.month), 0);
        int year = sharedPref.getInt(getString(R.string.year), 0);

        if (day !=0 && month!=0 && year != 0) {
            presenterParent.calculateAge(String.valueOf(day),String.valueOf(month),String.valueOf(year));
        }
    }

    private void init() {
        presenterParent= new PresenterParent(this);
    }

    private void grapUi() {
        ipAdress=(EditText)findViewById(R.id.address);
        TextDay=(EditText)findViewById(R.id.TextDay);
        TextMonth=(EditText)findViewById(R.id.TextMonth);
        TextYear=(EditText)findViewById(R.id.TextYear);
        AgeChild=(TextView)findViewById(R.id.AgeChild);
        buttonOk=(Button)findViewById(R.id.buttonOk);
        gallery=(Button)findViewById(R.id.galery);
        date=(LinearLayout)findViewById(R.id.date);
        coordinator=(CoordinatorLayout)findViewById(R.id.coordinatorLayout);
    }

    private void setListener(){
        setListenerTextDay();
        setListenerTextMonth();
    }

    private void setListenerTextMonth() {
        TextMonth.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean focus = false;
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if (!TextMonth.getText().toString().equals("") && Integer.parseInt(TextMonth.getText().toString()) > 0 &&
                            Integer.parseInt(TextMonth.getText().toString()) <= 12) {
                        focus =  true;
                        TextYear.requestFocus();
                    }
                }
                return focus;
            }
        });
    }


    private void setListenerTextDay() {
        TextDay.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean focus = false;
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if (!TextDay.getText().toString().equals("") && Integer.parseInt(TextDay.getText().toString()) > 0 &&
                            Integer.parseInt(TextDay.getText().toString()) < 30) {
                        focus =  true;
                        TextMonth.requestFocus();
                    }
                }
                return focus;
            }
        });
    }

    public void gotowe(View view) {


        //fajnie byloby zmienic jakos wpisywania daty urodzenia
        presenterParent.calculateAge(TextDay.getText().toString()
                , TextMonth.getText().toString()
                , TextYear.getText().toString());
        hideKeyboard();
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }



    @Override
    public void setAge(String age) {
        AgeChild.setTextColor(getResources().getColor(R.color.pink));
        AgeChild.setTypeface(null, Typeface.BOLD);
        AgeChild.setText(age);
        buttonOk.setVisibility(View.INVISIBLE);
        date.setVisibility(View.INVISIBLE);
        LinearLayout clouds=(LinearLayout)findViewById(R.id.clouds);
        clouds.setBackgroundResource(R.drawable.stopki);
    }

    @Override
    public void badData(String message) {
        Snackbar snackbar = Snackbar.make(coordinator,message, Snackbar.LENGTH_LONG)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });

// Changing message text color
        snackbar.setActionTextColor(Color.RED);
        snackbar.show();
    }
    public void saveIp(String ip){
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.saveIp), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.ip), ip);
        editor.commit();
    }
    @Override
    public void saveBirthday(int day, int month, int year) {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.savebirthday), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.day), day);
        editor.putInt(getString(R.string.month), month);
        editor.putInt(getString(R.string.year), year);

        editor.commit();
    }

    @Override
    public void intentConnect(String ip) {
        Intent intent = new Intent(this,Nanny.class);
        intent.putExtra("ip",ip);
        saveIp(ip);
        startActivity(intent);

    }

    public void calendar(View view) {
        int age=presenterParent.getCountMonth();
        if(age!=-1) {
            Intent intent = new Intent(this,Calendar.class);
            intent.putExtra("age", age);
            startActivity(intent);
        }else{
            badData("Wpisz date urodzenia dziecka");
        }
    }

    public void feeding(View view) {
        int age=presenterParent.getAgeForFeeding();
        if(age!=-1) {
            Intent intent = new Intent(this, Feeding.class);
            intent.putExtra("age", age);
            startActivity(intent);
        }else{
            badData("Wpisz date urodzenia dziecka");
        }
    }

    public void gallery(View view) {
        int age=presenterParent.getAgeForFeeding();
        if(age!=-1) {
            Intent intent = new Intent(this, Gallery_Child.class);
            intent.putExtra("age", age);
            startActivity(intent);
        }else{
            badData("Wpisz date urodzenia dziecka");
        }
    }

    public void connect(View view) {
        String ip=ipAdress.getText().toString();
        presenterParent.validateIp(ip);
    }
    private void checkIp() {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.saveIp), Context.MODE_PRIVATE);
        String ip = sharedPref.getString(getString(R.string.ip),"nic");
        if(!ip.equals("nic")){
            ipAdress.setText(ip);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_parent,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        DialogSettings dialogSettings = new DialogSettings();
        switch (item.getItemId()) {
            case R.id.action_settings : {
                Bundle bundle= new Bundle();
                bundle.putInt("flags",0);
                dialogSettings.setArguments(bundle);
                dialogSettings.setOnDialogListener(this);
                dialogSettings.show(getSupportFragmentManager(), DialogSettings.class.getSimpleName());

                return true;
            }
            case R.id.action_ip : {
                Bundle bundle= new Bundle();
                bundle.putInt("flags",1);
                dialogSettings.setArguments(bundle);
                dialogSettings.setOnDialogListener(this);
                dialogSettings.show(getSupportFragmentManager(), DialogSettings.class.getSimpleName());
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void delete(int position) {
    }

    @Override
    public void add(String message) {
    }

    @Override
    public void changeData(String day, String month, String year) {
        MyApplication application= (MyApplication)getApplicationContext();
        application.getDatabases().clearDatabases();
        presenterParent.calculateAge(day,month,year);
    }

    @Override
    public void changeIp(String ip) {
        saveIp(ip);
        ipAdress.setText(ip);
    }


}
