package com.example.sylwia.mobile_nanny.view.adapter;

import android.content.res.TypedArray;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.sylwia.mobile_nanny.view.fragment.FragmentGalleryViewPager;


public class AdapterGalleryViewPager extends FragmentStatePagerAdapter {
    private FragmentGalleryViewPager myFragment[];
    private TypedArray photos;

    public AdapterGalleryViewPager(FragmentManager fm, TypedArray photos) {
        super(fm);
        this.photos=photos;
        myFragment= new FragmentGalleryViewPager[photos.length()];
    }

    @Override
    public Fragment getItem(int position) {
        if(myFragment[position]==null) {
            myFragment[position] = new FragmentGalleryViewPager();
        }
        myFragment[position].setAdressPicture(photos.getResourceId(position,-1));
        return myFragment[position];
    }

    @Override
    public int getCount() {  //ile mam obrazkow(dlugosc fragmentu)
        return photos.length();
    }

}
