package com.example.sylwia.mobile_nanny.view.activity;


import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sylwia.mobile_nanny.R;
import com.example.sylwia.mobile_nanny.presenter.PresenterServer;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;


public class Child extends AppCompatActivity implements PresenterServer.OnReciveServer {


    private PresenterServer presenter;
    private TextView infoip;

    private android.hardware.Camera camera;
    private ViewCamera viewCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        infoip=(TextView)findViewById(R.id.tekstView);
        infoip.setText(getIpAddress());
        presenter = new PresenterServer(this);
        presenter.startServer();

        try{
            camera= android.hardware.Camera.open(findBackFacingCamera());
        }catch(Exception e){

        }
        if(camera != null){
            viewCamera = new ViewCamera(this,camera, presenter);
            FrameLayout frameLayout= (FrameLayout) findViewById(R.id.cameraView);
            frameLayout.addView(viewCamera);
        }
    }
    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> eNI = NetworkInterface.getNetworkInterfaces();
            while (eNI.hasMoreElements()){
                NetworkInterface nI = eNI.nextElement();
                Enumeration<InetAddress> eIA = nI.getInetAddresses();
                while (eIA.hasMoreElements()) {
                    InetAddress iA = eIA.nextElement();
                    if (iA.isSiteLocalAddress()) {
                        ip = ip + iA.getHostAddress() + "Przepisz adres IP";
                    }

                }

            }

        }catch (SocketException e) {
            e.printStackTrace();
            ip += "Error " + e.toString() + "\n";
        }

        return ip;
    }

    private MediaPlayer mp;

    @Override
    public void play(int index) {
        int[] graj={R.raw.bach,R.raw.elise_beethoven,R.raw.vivaldi};
        if(mp!=null){
            mp.release();
        }
        mp = MediaPlayer.create(this, graj[index]);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mp.start();
            }
        });
    }

    @Override
    public void stop() {
        mp.stop();
    }

    @Override
    public void sendImage() {
          viewCamera.sendImage();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "przeslij obraz", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void stopImage() {
        viewCamera.stopImage();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "stop obraz", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private int findBackFacingCamera() {

        int numOfCameras = android.hardware.Camera.getNumberOfCameras();
        int Idcamera = 0;
        for (int i = 0; i < numOfCameras; i++) {
            android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
            android.hardware.Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                Idcamera = i;
                break;
            }
        }
        return Idcamera;
    }


}
