package com.example.sylwia.mobile_nanny.model;


import android.app.Application;

import com.example.sylwia.mobile_nanny.view.activity.ClientParent;
import com.example.sylwia.mobile_nanny.view.activity.OnConnectClient;
import com.example.sylwia.mobile_nanny.view.activity.Server;

public class MyApplication extends Application { // wszystkie klasy moga sie dostac do niej przez kontext

    private DatabaseGreenDao greenDao;
    private ClientParent client;
    @Override
    public void onCreate() {
        super.onCreate();
        greenDao= new DatabaseGreenDao(getApplicationContext());

    }

    public DatabaseGreenDao getDatabases(){ //zwraca dostep do bazy danych
        synchronized (greenDao){
            return greenDao;
        }
    }

    public void startClient(String ip, OnConnectClient onConnectClient){
        client = new ClientParent(onConnectClient,ip);
    }
    public ClientParent getClient(){ //zwraca dostep do klienta
        return client;
    }



}
