package com.example.sylwia.mobile_nanny.presenter;

import com.example.sylwia.mobile_nanny.view.activity.SendImage;
import com.example.sylwia.mobile_nanny.view.activity.Server;

public class PresenterServer extends SendImage implements Server.OnReciveMessage{

    private OnReciveServer listener;

    public interface OnReciveServer {
        void play(int index);
        void stop();
        void sendImage();
        void stopImage();
    }

    public PresenterServer(OnReciveServer childServer) {
        this.listener = childServer;
    }

    public void startServer() {
        if(server==null) {
            server = new Server(this);
        }
        server.start();
    }


    @Override
    public void onRecive(String message) {
        if(String.valueOf(message.charAt(0)).equals("0")) {
            listener.play(Integer.parseInt(String.valueOf(message.charAt(1))));
        }else if(message.equals("1")){
            listener.stop();
        }
        else if (message.equals("image")) {
            listener.sendImage();
        }
        else if (message.equals("stopImage")) {
            listener.stopImage();
        }
    }
}
