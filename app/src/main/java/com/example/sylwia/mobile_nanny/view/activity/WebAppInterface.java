package com.example.sylwia.mobile_nanny.view.activity;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;


public class WebAppInterface {


    Context mContext;
    private int nr;
    /** Instantiate the interface and set the context */
    WebAppInterface(Context c, int nr) {
        mContext = c;
        this.nr=nr;
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public String showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
      return "mama "+nr;
    }
}
