package com.example.sylwia.mobile_nanny.view.activity;


import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.sylwia.mobile_nanny.R;
import com.example.sylwia.mobile_nanny.view.adapter.AdapterGallery;
import com.example.sylwia.mobile_nanny.view.adapter.AdapterGalleryViewPager;

public class Gallery_Child extends AppCompatActivity{
    private ViewPager imageSelected;
    private RecyclerView gallery;
    private int ageChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_child);
        getAge(getIntent().getExtras());
        grapUi();
        initRecycleView();
        setAdapter(getImageArray());
        setListenerRecycleView();

    }

    private void setListenerRecycleView() {
        gallery.addOnItemTouchListener(new OnRecyclerViewListener(getApplicationContext(), new OnRecyclerViewListener.OnClickListenerRecycler() {
            @Override
            public void onClick(int position, View view) {
                imageSelected.setCurrentItem(position);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }, gallery));
    }

    private TypedArray getImageArray() {
        if (ageChild<=6){
            return getResources().obtainTypedArray(R.array.gallery_image_0_6);
        }else if (ageChild>7 && ageChild<12){
            return getResources().obtainTypedArray(R.array.gallery_image_6plus);
        }
        return getResources().obtainTypedArray(R.array.gallery_image_12plus);
    }

    private void setAdapter(TypedArray imageArray) {
        AdapterGallery adapterGallery= new AdapterGallery(getApplicationContext(), imageArray);
        gallery.setAdapter(adapterGallery);
        AdapterGalleryViewPager adapterViewPager=new AdapterGalleryViewPager(getSupportFragmentManager(), imageArray);
        imageSelected.setAdapter(adapterViewPager);
    }

    private void initRecycleView() {
        gallery.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        gallery.setLayoutManager(llm);
    }

    private void grapUi() {
        gallery = (RecyclerView)findViewById(R.id.gallery1);
        imageSelected = (ViewPager)findViewById(R.id.viewPager);
    }

    private void getAge(Bundle extras) {
        if (extras != null) {
            ageChild=extras.getInt("age");
        }
    }



}
