package com.example.sylwia.mobile_nanny.presenter;

import android.text.format.Time;

public class PresenterParent {

    private OnPresenterParentListener onPresenterParentListener;
    private int volMonth;
    private int volYear;

    public interface OnPresenterParentListener{
        void setAge(String age);
        void badData(String message);
        void saveBirthday(int day, int month, int year);
        void intentConnect(String ip);
    }

    public PresenterParent(OnPresenterParentListener onPresenterParentListener){
        this.onPresenterParentListener=onPresenterParentListener;
        volMonth=-1;
        volYear=-1;
    }

    public void validateIp(String ip){
        String [] ipAdress= ip.split("\\.");
        int tabLength=ipAdress.length;
        boolean flag = true;
        if(tabLength==4){
            for(int i=0;i<tabLength;i++){
                if(Integer.parseInt(ipAdress[i])>255 && Integer.parseInt(ipAdress[i])<0){
                    onPresenterParentListener.badData("Błędne ip");
                    flag = false;
                    break;
                }
            }
            if(flag == true){
                onPresenterParentListener.intentConnect(ip);
            }
        }else{
            onPresenterParentListener.badData("Błędne IP");
        }
    }

    public void calculateAge(String TextDay, String TextMonth, String TextYear ) {

        try {
            int DayBirth = Integer.parseInt(TextDay);
            int MonthBirth = Integer.parseInt(TextMonth);
            int YearBirth = Integer.parseInt(TextYear);

            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            int todayYear = today.year;
            int todayMonth = today.month;
            int todayDay = today.monthDay;
            volYear = 0;
            volMonth = 0;
            todayMonth++;
            if (YearBirth <= todayYear) {
                volYear = todayYear - YearBirth;

                if (MonthBirth <= todayMonth) {
                    volMonth = todayMonth - MonthBirth;
                } else {
                    volYear--;
                    volMonth = 12 - (MonthBirth - todayMonth);
                }
                if (DayBirth > todayDay) {
                    if (volMonth == 0) {
                        volYear--;
                        volMonth = 11;
                    } else {
                        volMonth--;
                    }
                }
            }
            String age = "";
            if (volYear == 0) {
                age = volMonth + " mies";
            } else {
                age = volYear + " lat " + volMonth + " mies";
            }

            onPresenterParentListener.saveBirthday(DayBirth, MonthBirth, YearBirth);
            onPresenterParentListener.setAge(age);
        }catch(NumberFormatException e){
            onPresenterParentListener.badData("Podałeś złe dane");
        }
    }
    public int getAgeForFeeding(){
        if(volYear>0){
            return 12;
        }else if(volMonth>=0) {
            return volMonth;
        }
        return -1;
    }

    public int getCountMonth(){
        if(volMonth!=-1 && volYear!=-1){
            return volYear*12+volMonth;

        }return -1;
    }
}
