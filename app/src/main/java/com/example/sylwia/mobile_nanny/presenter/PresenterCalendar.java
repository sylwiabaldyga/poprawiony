package com.example.sylwia.mobile_nanny.presenter;


import android.content.Context;
import android.text.format.Time;

import com.example.sylwia.mobile_nanny.model.CalendarMonthInfo;
import com.example.sylwia.mobile_nanny.util.Constant;
import com.example.sylwia.mobile_nanny.view.activity.Calendar;

public class PresenterCalendar {
    private int currentYear;
    private int currentMonth;
    private int currentDay;
    private int showMonth;
    private int showYear;
    private OnCalendarListener onCalendarListener;
    private int weekDay;
    private CalendarMonthInfo currentMonthInfo;
    private CalendarMonthInfo nextMonth;
    private CalendarMonthInfo prevMonth;



    public PresenterCalendar(OnCalendarListener onCalendarListener) {
        this.onCalendarListener = onCalendarListener;
    }

    public void prevShowMonth(Context applicationContext) {
        showMonth--;
        if(showMonth==-1) {
            showYear--;
            showMonth=11;
        }
        CalendarMonthInfo pom=currentMonthInfo;
        onCalendarListener.showDate(Constant.getNameMonth(showMonth,applicationContext)+" "+ showYear);
        currentMonthInfo = prevMonth;
        nextMonth = pom;
        int countMonthPrev=getCountMonthPrev();
        prevMonth = new CalendarMonthInfo(countMonthPrev,firstDayMonthPrev(countMonthPrev,
                currentMonthInfo.getFirstDay()));

        onCalendarListener.showDaysInMonth( currentMonthInfo, nextMonth, prevMonth);
    }

    public void nextShowMonth(Context applicationContext) {
        showMonth++;
        if(showMonth==12){
            showYear++;
            showMonth=0;
        }
        CalendarMonthInfo pom=currentMonthInfo;
        onCalendarListener.showDate(Constant.getNameMonth(showMonth,applicationContext)+ " "+ showYear);
        currentMonthInfo = nextMonth;
        nextMonth = new CalendarMonthInfo(getCountMonthNext(),firstDayMonthNext(currentMonthInfo.getCountDays(),currentMonthInfo.getFirstDay()));
        prevMonth = pom;

        onCalendarListener.showDaysInMonth( currentMonthInfo, nextMonth, prevMonth);
    }

    public interface OnCalendarListener{
        void showDate(String date);
        void showDaysInMonth(CalendarMonthInfo numberDays,
                             CalendarMonthInfo numberDaysMonthNext,
                             CalendarMonthInfo numberDaysMonthPrev );


    }

    public void setCurrentDate(Context context){
        getCurrentTime();
        onCalendarListener.showDate(Constant.getNameMonth(currentMonth,context)+" "+ currentYear);
        currentMonthInfo= new CalendarMonthInfo(Constant.getDaysinMonth(currentMonth, currentYear),firstDayMonth());
        int countMonthPrev=getCountMonthPrev();
        nextMonth = new CalendarMonthInfo(getCountMonthNext(),firstDayMonthNext(currentMonthInfo.getCountDays(),currentMonthInfo.getFirstDay()));
        prevMonth = new CalendarMonthInfo(countMonthPrev,firstDayMonthPrev(countMonthPrev, currentMonthInfo.getFirstDay()));

        onCalendarListener.showDaysInMonth( currentMonthInfo, nextMonth, prevMonth);


    }

    private void getCurrentTime() {
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        currentYear = today.year;
        currentMonth = today.month;
        currentDay = today.monthDay;
        showMonth= currentMonth;
        showYear= currentYear;
        weekDay = today.weekDay;

    }


    private int getCountMonthNext(){
        int countDays=0;
        if(showMonth!=11) {
           countDays = Constant.getDaysinMonth(showMonth + 1, showYear);
        }else{
            countDays = Constant.getDaysinMonth(0,showYear+1);
        }
        return countDays;
    }

    private int getCountMonthPrev(){
        int countDays=0;
        if(showMonth!=0) {
            countDays = Constant.getDaysinMonth(showMonth - 1, showYear);
        }else{
            countDays = Constant.getDaysinMonth(11,showYear - 1);
        }
        return countDays;
    }


    private int firstDayMonth(){
        int firstDay=0;
        if(currentDay==1){
            return weekDay;
        }else if(currentDay<=7){
            firstDay=weekDay-(currentDay-1);
            if(firstDay<0){
                firstDay=7+firstDay;
            }
        }else {
             firstDay=currentDay%7;
            if(firstDay==0){
                firstDay=weekDay+1;
            }else{
                firstDay=weekDay-(firstDay-1);
                if(firstDay<0){
                    firstDay=7+firstDay;
                }
            }
        }
        return firstDay;
    }

    private
    int firstDayMonthNext(int countDayCurrentMonth, int weekDaycurrentMonth){
        int firstDay=0;
        switch (countDayCurrentMonth){
            case 31 :
                firstDay=(weekDaycurrentMonth+3)%7;
                break;
            case 30 :
                firstDay=(weekDaycurrentMonth+2)%7;
                break;
            case 29 :
                firstDay=(weekDaycurrentMonth+1)%7;
                break;
            case 28 :
                firstDay=weekDaycurrentMonth;
                break;
        }
        return firstDay;
    }

    private int firstDayMonthPrev(int countDayPrevMonth, int weekDaycurrentMonth){
        int firstDay=0;
        switch (countDayPrevMonth){
            case 31 :
                firstDay=(weekDaycurrentMonth-3);
                if(firstDay<0){
                    firstDay=7+firstDay;
                }
                break;
            case 30 :
                firstDay=(weekDaycurrentMonth-2);
                if(firstDay<0){
                    firstDay=7+firstDay;
                }
                break;
            case 29 :
                firstDay=(weekDaycurrentMonth-1);
                if(firstDay<0){
                    firstDay=7+firstDay;
                }
                break;
            case 28 :
                firstDay=weekDaycurrentMonth;
                break;
        }
        return firstDay;
    }


}
