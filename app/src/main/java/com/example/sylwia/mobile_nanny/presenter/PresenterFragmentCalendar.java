package com.example.sylwia.mobile_nanny.presenter;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.example.sylwia.mobile_nanny.model.CalendarMonthInfo;
import com.example.sylwia.mobile_nanny.model.DatabaseGreenDao;
import com.example.sylwia.mobile_nanny.model.MyApplication;
import com.example.sylwia.mobile_nanny.model.dataBase.NoteCalendar;

import java.util.ArrayList;
import java.util.List;

public class PresenterFragmentCalendar extends Fragment {  //headless fragment
    private int clcickedDay=-1;
    private List<NoteCalendar> daysWithData;
    private int month;
    private OnFragmentCalendarListener listener;

    public interface OnFragmentCalendarListener{
        void showDaysWithDatabase();
    }

    public void setListener(OnFragmentCalendarListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        MyApplication application= (MyApplication)getActivity().getApplicationContext();
        daysWithData = application.getDatabases().getNoteForMonth(month);
        listener.showDaysWithDatabase();
    }

    public void setClcickedDay(int clcickedDay) {
        this.clcickedDay = clcickedDay;
    }

    public int getClcickedDay() {
        return clcickedDay;
    }



    public boolean isDataInDay(int day){
        if(daysWithData == null || daysWithData.size()==0)
            return false;
        else {
            for(int i= 0; i < daysWithData.size();i++){
               if( daysWithData.get(i).getNumberDay()==day){
                   return true;
               }
            }

        }
        return false;
    }

    public void setDaysWithData(int currentItem){
        daysWithData.add(new NoteCalendar(null,currentItem, clcickedDay,""));
    }

    public void setMonth(int month) {
        this.month = month;
    }
}
