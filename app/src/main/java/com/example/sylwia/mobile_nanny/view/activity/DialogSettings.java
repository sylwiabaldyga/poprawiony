package com.example.sylwia.mobile_nanny.view.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.example.sylwia.mobile_nanny.R;

public class DialogSettings extends DialogFragment {

    private EditText day;
    private EditText month;
    private EditText year;
    private EditText ip;

    public void setOnDialogListener(OnDialogListener onDialogListener) {
        this.onDialogListener = onDialogListener;
    }

    private OnDialogListener onDialogListener;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view =inflater.inflate(R.layout.dialog_setting, null);
        final Bundle bundle =getArguments();
        showView(bundle ,view);
        builder.setView(view)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (bundle.getInt("flags") == 0) {
                            onDialogListener.changeData(
                                    day.getText().toString(),
                                    month.getText().toString(),
                                    year.getText().toString());

                        }else{
                            onDialogListener.changeIp(ip.getText().toString());
                        }
                    }
                })
                .setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        return builder.create();
    }

    private void showView(Bundle bundle,View view) {

        if(bundle.getInt("flags")==0){
            view.findViewById(R.id.edit_Ip).setVisibility(View.INVISIBLE);
            day= (EditText) view.findViewById(R.id.TextDay);
            month= (EditText) view.findViewById(R.id.TextMonth);
            year= (EditText) view.findViewById(R.id.TextYear);
            setListener();
        }else{
            view.findViewById(R.id.edit_date).setVisibility(View.INVISIBLE);
            ip = (EditText) view.findViewById(R.id.edit_Ip);
        }
    }

    private void setListener() {
        setListenerTextDay();
        setListenerTextMonth();
    }
    private void setListenerTextMonth() {
        month.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean focus = false;
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if (!month.getText().toString().equals("") && Integer.parseInt(month.getText().toString()) > 0 &&
                            Integer.parseInt(month.getText().toString()) <= 12) {
                        focus =  true;
                        year.requestFocus();
                    }
                }
                return focus;
            }
        });
    }


    private void setListenerTextDay() {
        day.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean focus = false;
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if (!day.getText().toString().equals("") && Integer.parseInt(day.getText().toString()) > 0 &&
                            Integer.parseInt(day.getText().toString()) < 30) {
                        focus =  true;
                        month.requestFocus();
                    }
                }
                return focus;
            }
        });
    }


}
