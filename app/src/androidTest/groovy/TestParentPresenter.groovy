
import com.example.sylwia.mobile_nanny.presenter.PresenterParent
import spock.lang.Specification


class TestParentPresenter extends Specification {



    def "Validate Ip"() {
        given:
        def listener = Mock(PresenterParent.OnPresenterParentListener.class)
        def presenter = new PresenterParent(listener)
        def ip = "1.1.1.1"
        when:
        presenter.validateIp(ip)
        then:
        1*listener.intentConnect(ip)
    }

    def "Bad Validate Ip"() {
        given:
        def listener = Mock(PresenterParent.OnPresenterParentListener.class)
        def presenter = new PresenterParent(listener)
        def ip = "1.1.1"
        when:
        presenter.validateIp(ip)
        then:
        1*listener.badData("Błędne IP")
    }

    def "Calculate age child"(){
        given:
            def listener = Mock(PresenterParent.OnPresenterParentListener.class)
            def presenter = new PresenterParent(listener)
            def day = "12"
            def month = "8"
            def year = "2015"
            def age = "10 mies"
        when:
            presenter.calculateAge(day,month, year)
        then:
            1*listener.setAge(age)
    }



}
