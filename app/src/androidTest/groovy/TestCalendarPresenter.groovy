import com.andrewreitz.spock.android.WithContext
import com.example.sylwia.mobile_nanny.presenter.PresenterCalendar
import com.example.sylwia.mobile_nanny.util.Constant
import spock.lang.Specification


class TestCalendarPresenter extends Specification {

    @WithContext def context

    def " Current time"() {
        given:
        def listener = Mock(PresenterCalendar.OnCalendarListener.class)
        def presenter = new PresenterCalendar(listener)
        def month = "Czerwiec"
        def year = "2016"
        when:
            presenter.setCurrentDate(context)
        then:
            1*listener.showDate(month+" "+year)

    }



}