import com.andrewreitz.spock.android.WithContext
import com.example.sylwia.mobile_nanny.util.Constant
import spock.lang.Specification


class ConstantTest extends Specification {

    @WithContext def context

    def " Name month"() {
        given: "variable initialization"
        def result
        when: "Get value of method for the month number 2"
        result = Constant.getNameMonth(2, context)
        then: "compare results"
        result == "Marzec"
    }

    def "Number of days in Month"(){
        given:"variable initialization"
        def result
        def month = 5
        def year = 2016
        when: "Get value of method for the month number 5 and year 2016"
        result= Constant.getDaysinMonth(month,year)
        then:
        result == 30

    }

}