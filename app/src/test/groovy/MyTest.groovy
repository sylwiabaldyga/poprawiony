import android.app.Application
import android.content.Context
import android.test.mock.MockContext
import com.example.sylwia.mobile_nanny.presenter.PresenterParent
import com.example.sylwia.mobile_nanny.util.Constant
import com.example.sylwia.mobile_nanny.view.activity.MainActivity
//import org.mockito.Mockito
import spock.lang.Specification
import spock.lang.Unroll

class MyTest extends Specification{

    @Unroll
    def "TestConstantClass"(){
        given: "initialize Variable"
        //def contex = Mock(com.example.sylwia.mobile_nanny.view.activity.Calendar.class)
        def nameMonth

        when: "adding variable"
        nameMonth=Constant.getDaysinMonth(0,2015)
        then: "styczen ma 31 dni"

        nameMonth==31
    }
    @Unroll
    def "test constant"(){
        given: "initialize Variable"
        //@WithContext def context
        def context = new MockContext()
        def constContext= context.getApplicationContext()
//Mockito.mock(Application.class)
        def monthName;
        when: "Which is the month"
        monthName= Constant.getNameMonth(6,constContext)
        then: "And ??"
        monthName == "Czerwiec"


    }
    /*@Unroll
    def "Test"() {
        given: "initialize Variable"
        //def listener = Mock(PresenterCalendar.OnCalendarListener)

        //def listener = Mock(PresenterCalendar.OnCalendarListener)
        //def presenter = new PresenterCalendar(listener)
        def presenter = Mockito.mock(PresenterParent)
        def firstDay
        when: "First Day Month"
        firstDay=presenter.calculateAge("09", "07", "2015")
    }*/
}