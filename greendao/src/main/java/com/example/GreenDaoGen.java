package com.example;


import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class GreenDaoGen {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(1, "com.example.sylwia.mobile_nanny.model.dataBase"); //wersja 1?
        createTable(schema);

        new DaoGenerator().generateAll(schema, "/home/sylwia/Pulpit/Android/MobileNanny/app/src/main/java");
    }

    private static void createTable(Schema schema) {
        Entity notes = schema.addEntity("NoteCalendar");
        notes.addIdProperty().primaryKey();
        notes.addIntProperty("numberMonth");// nr view peager karta
        notes.addIntProperty("numberDay");
        notes.addStringProperty("note").notNull();
    }
}
